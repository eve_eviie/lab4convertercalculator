package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener{
	
	float intRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button bn1 = (Button)findViewById(R.id.btnConvInt);
		bn1.setOnClickListener(this);
		
		Button bn2 = (Button)findViewById(R.id.btnSettingInt);
		bn2.setOnClickListener(this);
		
		TextView tvIntRATE = (TextView)findViewById(R.id.tvIntRate);
		intRate = Float.parseFloat(tvIntRATE.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		
		if(id == R.id.btnConvInt){
			EditText etDep = (EditText)findViewById(R.id.etDep);
			EditText etTime = (EditText)findViewById(R.id.etTime);
			TextView tvAmount = (TextView)findViewById(R.id.tvAmount);
			String res = "";
			try {
				float p = Float.parseFloat(etDep.getText().toString());
				float t = Float.parseFloat(etTime.getText().toString());
				float amount = (float) (p * Math.pow((1+(intRate/100)), t));
				res = String.format(Locale.getDefault(), "%.2f", amount);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvAmount.setText("$ "+res);
			
		}
		else if (id == R.id.btnSettingInt) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("intRate", intRate);
			startActivityForResult(i, 9999); //9999 is requested code
		}
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			intRate = data.getFloatExtra("intRate", 10.0f);
			TextView tvIntRate = (TextView)findViewById(R.id.tvIntRate);
			tvIntRate.setText(String.format(Locale.getDefault(), "%.2f", intRate));
		}
	}

}
