package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate;
	float intRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
		intRate = i.getFloatExtra("intRate", 10.0f);
		
		EditText etIntRate = (EditText)findViewById(R.id.etInt);
		etIntRate.setText(String.format(Locale.getDefault(), "%.2f", intRate));
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Intent data = new Intent();
		
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		try {
			float r = Float.parseFloat(etRate.getText().toString());
			//Create an intent to send the value back
			//Attach a value to the intent
			data.putExtra("exchangeRate", r);
			//Set the intent as the result of this activity
			//this.setResult(RESULT_OK, data);
			//End this activity
			//this.finish();
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
		
		
		EditText etIntRate = (EditText)findViewById(R.id.etInt);
		try {
			float ir = Float.parseFloat(etIntRate.getText().toString());
			//Create an intent to send the value back
			//Intent data = new Intent();
			//Attach a value to the intent
			data.putExtra("intRate", ir);
			
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
		
		
		//Set the intent as the result of this activity
		this.setResult(RESULT_OK, data);
		//End this activity
		this.finish();
		
	}

}
